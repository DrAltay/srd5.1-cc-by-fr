# SRD5.1 CC-BY FR

## Présentation

Voici le SRD 5.1 du système **Donjons & Dragons** en français et sous licence Creative Commons BY. Il s'agit d'une traduction effectuée par Deepl à partir du [document fourni par oznogon](https://github.com/oznogon/cc-srd5) et basé sur un glossaire (lui aussi en CC-BY) conçu par mes soins en prenant comme référence celui d' [AideDD](https://www.aidedd.org/download.php?file=Glossaire&ext=pdf).

## TODO :

- [ ] Réviser la traduction.

- [ ] Réviser le chapitrage.

- [ ] Diviser le documents en plusieurs sections logiques et inclure un script de compilation ?

- [ ] Correction d'erreurs de traduction sur les alignements que j'ai oublié d'ajouter au glossaire.

- [X] Trouver un terme pour remplacer "race".

- [X] Changer Rhapsode pour Barde.

- [X] Changer Cacodémurge pour Invocateur.

- [X] Changer Mège pour Guérisseur.

- [X] En réflexion : changer Consacré pour béni/bénédiction.

- [X] En réflexion : du Péracosme → des Autres Mondes

- [X] En réflexion : Arnaqueur → revenir à Escroc (Roublard) ? J'ai aussi pensé à Aigrefin, mais j'ai peur que ce ne soit que peu évocateur.

## Documents

*lexique.csv* : le glossaire au format csv.

*srd-ccby-fr.md* : le document de référence au format markdown.

*srd-ccby-fr.html* : le document au format html.

*srd-ccby-fr.odt* : le document au format Libre Office Writer.

*srd-ccby-fr.pdf* : le document au format pdf.

## Explication de quelques choix de traduction

*Dark One* → **L'Oscur** : Mot du moyen-français équivalent à obscur (qui me semblait trop commun pour désigner une entité spécifique).

*Eldritch* → **Traumaturgique** : C'est un mot difficile à traduire. Le terme "occulte" est associé au savoir là ou le terme Eldritch renvoie à l'étrangeté, à l'inquiétant, en lien, dans la culture contemporaine, avec l'horreur lovecraftienne. Pour essayer de préserver cette dimension j'ai construis ce mot à partir de "trauma" et du "ergo" grec qui signifie "travail", ce qui pourrait donner quelque chose comme "œuvrer pour le trauma".

*Elemental* → **Élémental** : la traduction française actuelle "élémentaire" me semble eronnée. Élémentaire désigne ce qui concerne les élements, là où l'élémental désigne spécifiquement les esprits constitués d'élements.

*Féérie* → **Aspiolie** : Aspiole signifie Fée en moyen-français.

*Shadowfell* → **Faillombre** : De même que pour drakéide, je ne sais pas si Gisombre est une formulation protégée aussi ai-je cherché une forme alternative.

*Sorcerer* → **Sorcier** : la traduction actuelle par "ensorceleur" ne me semblait pas très heureuse et source de confusion. Pour *Warlock* a été choisit **Invocateur**.

*Underdark* → **Ténèbres Inférieurs** : Si vous avez une meilleure idée...
